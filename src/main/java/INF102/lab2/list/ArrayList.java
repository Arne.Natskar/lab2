package INF102.lab2.list;

import java.io.ObjectStreamException;
import java.util.Arrays;

public class ArrayList<T> implements List<T> {
	public static final int DEFAULT_CAPACITY = 10;
	private int n;
	private Object elements[];

	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
	@Override
	public T get(int index) {
		if (index >= size() || index < 0) {
			throw new IndexOutOfBoundsException("index wrong: " + index + ", size is: "+size());
		}
		return (T) elements[index];
	}
	@Override
	public void add(int index, T element) {
		if ((index < 0) || (index > size())) {
			throw new IndexOutOfBoundsException("Could nto  add element to "+index+" to the list "+ size());
		}

		if (elements.length < size()+1) {
			System.out.println(Arrays.toString(elements));
			increaseSizeList();
		}
		//System.out.println(Arrays.toString(elements));
		if(index == size()){
			elements[index] = element;
			n++;
		}
		else {
			Object nextObject = get(index);
			Object lastObject;
			elements[index] = element;
			n++;
			for (int k = index + 1; k < size(); k++) {
				lastObject = elements[k];
				elements[k] = nextObject;
				nextObject = lastObject;
			}
		}
	}
	public void increaseSizeList (){
		Object[] newElements = new Object[elements.length * 2];
		for (int i = 0; i < elements.length; i++) {
			newElements[i] = get(i);
		}
		elements = newElements;
	}
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}
}